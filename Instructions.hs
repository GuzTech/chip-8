module Instructions where
import Clash.Prelude
import Types

data Instr = SYS Addr
            | CLS
            | RET
            | JP Addr
            | CALL Addr
            | SEVB Vx Byte
            | SNEVB Vx Byte
            | SEVV Vx Vx
            | LDB Vx Byte
            | ADDVB Vx Byte
            | LDV Vx Vx
            | OR Vx Vx
            | AND Vx Vx
            | XOR Vx Vx
            | ADDVV Vx Vx
            | SUB Vx Vx
            | SHR Vx Vx
            | SUBN Vx Vx
            | SHL Vx Vx
            | SNEVV Vx Vx
            | LDI Addr
            | JPV Addr
            | RND Vx Byte
            | DRW Vx Vx Nibble
            | SKP Vx
            | SKNP Vx
            | LDVD Vx
            | LDVK Vx
            | LDDV Vx
            | LDSV Vx
            | ADDI Vx
            | LDFV Vx
            | LDBV Vx
            | LDIV Vx
            | LDVI Vx
            deriving (Show, Eq)

instance BitPack Instr where
    type BitSize Instr = 16
    pack a = case a of
        SYS a       -> 0x0 ++# pack a
        CLS         -> 0x00E0
        RET         -> 0x00EE
        JP a        -> 0x1 ++# pack a
        CALL a      -> 0x2 ++# pack a
        SEVB v b    -> 0x3 ++# pack v  ++# pack b
        SNEVB v b   -> 0x4 ++# pack v  ++# pack b
        SEVV vx vy  -> 0x5 ++# pack vx ++# pack vy ++# (0x0 :: BV4)
        LDB v b     -> 0x6 ++# pack v  ++# pack b
        ADDVB v b   -> 0x7 ++# pack v  ++# pack b
        LDV vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0x0 :: BV4)
        OR vx vy    -> 0x8 ++# pack vx ++# pack vy ++# (0x1 :: BV4)
        AND vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0x2 :: BV4)
        XOR vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0x3 :: BV4)
        ADDVV vx vy -> 0x8 ++# pack vx ++# pack vy ++# (0x4 :: BV4)
        SUB vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0x5 :: BV4)
        SHR vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0x6 :: BV4)
        SUBN vx vy  -> 0x8 ++# pack vx ++# pack vy ++# (0x7 :: BV4)
        SHL vx vy   -> 0x8 ++# pack vx ++# pack vy ++# (0xE :: BV4)
        SNEVV vx vy -> 0x9 ++# pack vx ++# pack vy ++# (0x0 :: BV4)
        LDI a       -> 0xA ++# pack a
        JPV a       -> 0xB ++# pack a
        RND v b     -> 0xC ++# pack v  ++# pack b
        DRW vx vy n -> 0xD ++# pack vx ++# pack vy ++# pack n
        SKP v       -> 0xE ++# pack v  ++# (0x9E :: BV8)
        SKNP v      -> 0xE ++# pack v  ++# (0xA1 :: BV8)
        LDVD v      -> 0xF ++# pack v  ++# (0x07 :: BV8)
        LDVK v      -> 0xF ++# pack v  ++# (0x0A :: BV8)
        LDDV v      -> 0xF ++# pack v  ++# (0x15 :: BV8)
        LDSV v      -> 0xF ++# pack v  ++# (0x18 :: BV8)
        ADDI v      -> 0xF ++# pack v  ++# (0x1E :: BV8)
        LDFV v      -> 0xF ++# pack v  ++# (0x29 :: BV8)
        LDBV v      -> 0xF ++# pack v  ++# (0x33 :: BV8)
        LDIV v      -> 0xF ++# pack v  ++# (0x55 :: BV8)
        LDVI v      -> 0xF ++# pack v  ++# (0x65 :: BV8)
    unpack bv = o where
        opc         = unpack (slice d15 d12 bv) :: Nibble
        rest        = slice d11 d0 bv
        a           = unpack rest :: Addr
        (v, b)      = unpack rest :: (Vx, Byte)
        (vx, vy, n) = unpack rest :: (Vx, Vx, Nibble)
        o = case (opc, rest) of
            (0x0, 0x0E0)    -> CLS
            (0x0, 0x0EE)    -> RET
            (0x0, _)        -> SYS a
            (0x1, _)        -> JP a
            (0x2, _)        -> CALL a
            (0x3, _)        -> SEVB v b
            (0x4, _)        -> SNEVB v b
            (0x5, _)        -> SEVV vx vy
            (0x6, _)        -> LDB v b
            (0x7, _)        -> ADDVB v b
--            (8, (_, 0 :: BitVector 4)) -> LDV vx vy
            (0x8, r)    -> o where
                n = slice d3 d0 r
                o = case (vx, vy, n) of
                    (_, _, 0x0) -> LDV vx vy
                    (_, _, 0x1) -> OR vx vy
                    (_, _, 0x2) -> AND vx vy
                    (_, _, 0x3) -> XOR vx vy
                    (_, _, 0x4) -> ADDVV vx vy
                    (_, _, 0x5) -> SUB vx vy
                    (_, _, 0x6) -> SHR vx vy
                    (_, _, 0x7) -> SUBN vx vy
                    (_, _, 0xE) -> SHL vx vy
            (0x9, _)        -> SNEVV vx vy
            (0xA, _)        -> LDI a
            (0xB, _)        -> JPV a
            (0xC, _)        -> RND v b
            (0xD, _)        -> DRW vx vy n
            (0xE, _)        -> SKNP v
            (0xF, r)    -> o where
                b = slice d7 d0 r
                o = case (v, b) of
                    (_, 0x07)   -> LDVD v
                    (_, 0x0A)   -> LDVK v
                    (_, 0x15)   -> LDDV v
                    (_, 0x18)   -> LDSV v
                    (_, 0x1E)   -> ADDI v
                    (_, 0x29)   -> LDFV v
                    (_, 0x33)   -> LDBV v
                    (_, 0x55)   -> LDIV v
                    (_, 0x65)   -> LDVI v
