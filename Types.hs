module Types where
import Clash.Prelude

type Addr   = Unsigned 12
type Vx     = Unsigned 4
type Byte   = Unsigned 8
type Nibble = Unsigned 4
type BV4    = BitVector 4
type BV8    = BitVector 8
